<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/logout', [AuthController::class, 'logout'])
    ->middleware('auth:sanctum');


Route::get('/login', [AuthController::class, 'authenticate']);

Route::resource('tasks', TaskController::class)->only('index', 'store');

Route::put('/tasks/{task}/complete', [TaskController::class, 'complete'])
    ->middleware('adminAPI');

Route::put('/tasks/{task}/statusNew', [TaskController::class, 'statusNew'])
    ->middleware('adminAPI');

Route::put('/tasks/{task}/description', [TaskController::class, 'updateDescription'])
    ->middleware('adminAPI');
