<?php

namespace Database\Seeders;

use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminAddSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        DB::table('roles')->insert(
            ['id' => 1, 'name' => 'admin', 'slug' => 'admin']
        );
        User::create([
            'name' => 'admin',
            'email' => 'astapbender1@yandex.ru',
            'password' => '123',
            'roles_id' => 1,
        ]);
    }
}
