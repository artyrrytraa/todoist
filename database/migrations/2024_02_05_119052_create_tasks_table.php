<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        try {
            Schema::create('tasks', function (Blueprint $table) {
                $table->id();
                $table->foreignId('users_id')->nullable()->constrained();
                $table->text('name_user');
                $table->text('email');
                $table->text('description');
                $table->foreignId('task_statuses_id')->constrained();
                $table->timestamps();
            });
        } catch (\Exception $e) {
            $this->down();
            throw $e;
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
