<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'email',
        'name_user',
        'task_statuses_id'

    ];

    public function status()
    {
        return $this->hasOne(TaskStatus::class, 'id', 'task_statuses_id');
    }
 
}
