<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskStatus extends Model
{
    use HasFactory;

    protected $table = 'task_statuses';

    public function task()
    {
        return $this->belongsTo(Task::class, 'task_statuses_id', 'id');
    }
}
