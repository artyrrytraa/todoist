<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    public function authenticate(Request $request)
    {
        $params = $request->validate([
            'name' => 'required',
            'password' => 'required'
        ]);

        if (Auth::attempt(['name' => $params['name'], 'password' => $params['password']])) {
            $user = Auth::user();
            $token = $user->createToken('Personal Access Token')->plainTextToken;
            return response()->json(['token' => $token, 'user' => new UserResource($user)]);
        }
        return response()->json(['error' => 'Неверный логин или пароль'], 401);
    }

    public function logout(Request $request)
    {
        $request->user('sanctum')->currentAccessToken()->delete();
        return  response()->json(['success' => 'ok']);
    }
}
