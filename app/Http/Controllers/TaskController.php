<?php

namespace App\Http\Controllers;

use App\Http\Resources\TaskCollection;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use App\Models\TaskStatus;
use App\Services\TaskServices;
use Illuminate\Http\Request;

class TaskController extends Controller
{


    protected $taskServices;
    const PER_PAGE = 3;


    public function __construct(TaskServices $taskServices)
    {
        $this->taskServices = $taskServices;
    }

    public function index(Request $request)
    {


        $sortBy = $request->query('sort_by', 'name_user');
        $sortDirection = $request->query('sort_direction', 'asc');

        return $this->taskServices->paginate(self::PER_PAGE, $sortBy, $sortDirection);
    }

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'name_user' => 'required|max:255',
            'email' => 'required|email',
            'description' => 'required',
        ]);

        $task =  new TaskResource($this->taskServices->store($validatedData));

        return response()->json($task, 201);
    }

    public function updateDescription(Request $request, Task $task)
    {

        $data = $request->validate([
            'description' => 'required',
        ]);

        $description = $data['description'];

        $this->taskServices->updateDescription($task, $description);

        return response()->json($task);
    }


    public function complete(Request $request, Task $task)
    {
        return new TaskResource($this->taskServices->complete($task));
    }

    public function statusNew(Request $request, Task $task)
    {
        return new TaskResource($this->taskServices->statusNew($task));
    }


    /** 
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     */


    /**
     * Display the specified resource.
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Task $task)
    {
    }

    /**
     * Update the specified resource in storage.
     */


    public function update(Request $request)
    {
    }



    /**
     * Remove the specified resource from storage.
     */

    public function destroy(Task $task)
    {
        //
    }
}
