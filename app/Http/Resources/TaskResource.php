<?php

namespace App\Http\Resources;

use App\Models\Task;
use App\Models\TaskStatus;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name_user' => $this->name_user,
            'email' => $this->email,
            'description' => $this->description,
            'status' => $this->status instanceof TaskStatus ? $this->status->slug : $this->status,
        ];
    }
}
