<?php

namespace App\Services;

use App\Http\Resources\TaskResource as ResourcesTaskResource;
use App\Models\Contact;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use App\Services\TaskStatusService;
use App\Models\TaskStatus;
use Illuminate\Support\Facades\Auth;

class TaskServices
{
    protected $sortableFields = ['created_at', 'name_user', 'status', 'email'];
    protected $sortDirectionAllowed = ['asc', 'desc'];


    public function paginate(int $perPage, $sortBy, $sortDirection)
    {

        if (!in_array($sortBy, $this->sortableFields))
            $sortBy = 'name_user';

        if (!in_array(strtolower($sortDirection), $this->sortDirectionAllowed))
            $sortDirection = 'asc';

        if ($perPage < 1)
            throw ('$perPage в TaskServices::paginate должен быть >0');


        return   TaskResource::collection(Task::query()
            ->join('task_statuses', 'tasks.task_statuses_id', '=', 'task_statuses.id')
            ->select('tasks.*', 'task_statuses.slug as status')
            ->orderBy($sortBy, $sortDirection)
            ->paginate($perPage));
    }

    public function store(array $params): Task
    {
        $params['task_statuses_id'] = TaskStatusService::getId(TaskStatusService::SLUG_NEW);
        if (Auth::check())
            $params['users_id'] = Auth::user()->id;


        return  Task::create($params);
    }

    public function updateDescription(Task $task, $description)
    {
        $task->description = $description;
        $task->save();
        return $task;
    }

    public function complete(Task $task)
    {
        $task->task_statuses_id = TaskStatusService::getId(TaskStatusService::SLUG_COMPLETED);
        $task->save();
        return $task;
    }
    public function statusNew(Task $task)
    {
        $task->task_statuses_id = TaskStatusService::getId(TaskStatusService::SLUG_NEW);
        $task->save();
        return $task;
    }
}
