<?php

namespace App\Services;

use App\Http\Resources\TaskResource as ResourcesTaskResource;
use App\Models\Contact;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use App\Models\TaskStatus;
use Exception;
use LaravelJsonApi\Core\Exceptions\JsonApiException;

class TaskStatusService
{

    const SLUG_STATUSES = [
        'new',
        'completed'
    ];

    const SLUG_NEW = 'new';
    const SLUG_COMPLETED = 'completed';

    public static function all()
    {
        return  TaskStatus::all();
    }

    public static function getId($slug)
    {
        if (!in_array($slug, self::SLUG_STATUSES))
            throw   JsonApiException::error("Данного статуса не существует");
        return  TaskStatus::where('slug', $slug)->firstOrFail()->id;
    }
}
