import { reactive, toRefs } from 'vue';

const state = reactive({
    user: null
});





export function useUserStore() {
    const setUser = (user) => {
        state.user = user;
        localStorage.setItem('user', JSON.stringify(user));
    };

    const getUser = () => {
        if (!state.user) {
            state.user = JSON.parse(localStorage.getItem('user'));
        }
        return state.user;
    };

    const getToken = () => {
        if (localStorage.getItem('token'))
            return localStorage.getItem('token');
    };

    const initUSer = () => {
        if (!state.user && localStorage.getItem('user')) {
            state.user = JSON.parse(localStorage.getItem('user'));
        }

    };

    const clearUser = () => {
        if (state.user) {
            state.user = null;
            localStorage.removeItem('user')
        }

    };
    const isAdmin = () => {
        if (state.user) {
            return state.user.role == 'admin';
        }
        return false;
    };
    const isAuth = () => {
        if (state.user) {
            return state.user;
        }
        return false;
    };

    return { ...toRefs(state), setUser, getUser, clearUser, isAdmin, initUSer, isAuth, getToken };
}   